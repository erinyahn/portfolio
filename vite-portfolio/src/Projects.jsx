import React, { useState, useEffect, useRef } from "react";
import next from "./img/next.png";
import prev from "./img/prev.png";
import Character from "./Character.jsx";
import "./Projects.css";
import left from "./img/left.png";
import right from "./img/right.png";
import up from "./img/up.png";
import down from "./img/down.png";
import NPC from "./NPC.jsx";

const Projects = ({ projectsData, currentProject, handleProjectChange }) => {
    const [showOverlay, setShowOverlay] = useState(true);
    const [descIndex, setDescIndex] = useState(0);
    const prevProjectIndex = currentProject === 0 ? projectsData.length - 1 : currentProject - 1;
    const nextProjectIndex = currentProject === projectsData.length - 1 ? 0 : currentProject + 1;
    const videoRef = useRef(null);

    useEffect(() => {
        const video = videoRef.current;

        const handleVideoLoad = () => {
        video.play().catch((error) => {
            console.error('Error playing video:', error);
        });
        };

        if (video) {
        video.load();
        video.addEventListener('loadedmetadata', handleVideoLoad);

        return () => {
            video.removeEventListener('loadedmetadata', handleVideoLoad);
        };
        }
    }, [projectsData[currentProject].mov]);

    useEffect(() => {
        setDescIndex(0);
    }, [projectsData]);

    useEffect(() => {
        let interval;

        if (!showOverlay && projectsData[currentProject].bottom_desc) {
            interval = setInterval(() => {
                setDescIndex((prevIndex) => (prevIndex + 1) % projectsData[currentProject].bottom_desc.length);
            }, 3000); 
        }

        return () => clearInterval(interval);
    }, [showOverlay, currentProject, projectsData]);

    const handleButtonClick = (direction) => {
        switch (direction) {
            case "up":
                window.dispatchEvent(new KeyboardEvent("keydown", { key: "ArrowUp" }));
                setTimeout(() => {
                    window.dispatchEvent(new KeyboardEvent("keyup", { key: "ArrowUp" }));
                }, 400);
                break;
            case "down":
                window.dispatchEvent(new KeyboardEvent("keydown", { key: "ArrowDown" }));
                setTimeout(() => {
                    window.dispatchEvent(new KeyboardEvent("keyup", { key: "ArrowDown" }));
                }, 1000);
                break;
            case "left":
                window.dispatchEvent(new KeyboardEvent("keydown", { key: "ArrowLeft" }));
                setTimeout(() => {
                    window.dispatchEvent(new KeyboardEvent("keyup", { key: "ArrowLeft" }));
                }, 100);
                break;
            case "right":
                window.dispatchEvent(new KeyboardEvent("keydown", { key: "ArrowRight" }));
                setTimeout(() => {
                    window.dispatchEvent(new KeyboardEvent("keyup", { key: "ArrowRight" }));
                }, 100);
                break;
            default:
                break;
        }
    };

    const handleNPCStartClick = () => {
        setShowOverlay(false);
    };

    return (
        <section>
            <div className="project-container">
                <div className="project">
                    <div className="left-side">
                        <div className="grid-container">
                            <div className="controller">
                                <div className="empty-cell"></div>
                                <img className="up" src={up} alt="Up" onClick={() => handleButtonClick("up")} />
                                <div className="empty-cell"></div>
                                <img className="left" src={left} alt="Left" onClick={() => handleButtonClick("left")} />
                                <div className="middle"></div>
                                <img className="right" src={right} alt="Right" onClick={() => handleButtonClick("right")} />
                                <div className="empty-cell"></div>
                                <img className="down" src={down} alt="Down" onClick={() => handleButtonClick("down")} />
                            </div>
                        </div>
                        <div className="prnxt-buttons">
                            <img
                                className="prev"
                                src={prev}
                                alt="Previous"
                                onClick={() => handleProjectChange("prev")}
                                style={{ transform: "rotate(-20deg)" }}
                            />
                            <img
                                className="next"
                                src={next}
                                alt="Next"
                                onClick={() => handleProjectChange("next")}
                                style={{ transform: "rotate(-20deg)" }}
                            />
                        </div>
                    </div>
                    <div className="project-title-and-image">
                        <h2 className="proj-title">
                            {showOverlay ? "PROJECTS" : `PROJECT: ${projectsData[currentProject].title}`}
                        </h2>
                        {projectsData[currentProject].mov && (
                        <div className="project-image-container">
                            <video
                                ref={videoRef}
                                autoPlay
                                loop
                                muted
                                playsInline 
                                preload="auto"
                                className="project-image"
                                onCanPlay={() => videoRef.current.play()}
                            >
                                <source src={projectsData[currentProject].mov} type="video/mp4" />
                            </video>
                                <div className={showOverlay ? "overlay-container" : ""}>
                                    {showOverlay && (
                                        <div className="initial-intro">
                                            <NPC onStartClick={handleNPCStartClick} />
                                        </div>
                                    )}
                                    {!showOverlay && (
                                        <div className="overlay">
                                            <p className="project-description">
                                                {projectsData[currentProject].description && (
                                                    <>
                                                        <strong>Description:</strong>{" "}
                                                        {projectsData[currentProject].description}
                                                        <br />
                                                    </>
                                                )}
                                                {projectsData[currentProject].api && (
                                                    <>
                                                        <strong>API:</strong> {projectsData[currentProject].api}
                                                        <br />
                                                    </>
                                                )}
                                            </p>
                                            <div className="icon-container">
                                                {projectsData[currentProject].icons &&
                                                    projectsData[currentProject].icons.map((icon, index) => (
                                                        <img
                                                            key={index}
                                                            src={icon}
                                                            alt={`icon-${index}`}
                                                            className="tech-icon"
                                                        />
                                                    ))}
                                            </div>
                                            <div className="link-container">
                                                {projectsData[currentProject].code_link && (
                                                    <a
                                                        href={projectsData[currentProject].code_link}
                                                        target="_blank"
                                                        rel="noopener noreferrer"
                                                        className="link text-change"
                                                    >
                                                        <i className="fab fa-gitlab"></i> Code
                                                    </a>
                                                )}
                                                {projectsData[currentProject].live_link && (
                                                    <a
                                                        href={projectsData[currentProject].live_link}
                                                        target="_blank"
                                                        rel="noopener noreferrer"
                                                        className="link text-change"
                                                    >
                                                        <i className="fas fa-external-link-alt"></i> Live
                                                    </a>
                                                )}
                                            </div>
                                        </div>
                                    )}
                                </div>
                                {!showOverlay && (<Character />)}
                                {!showOverlay && <div>
                                    {projectsData[currentProject].bottom_desc && (
                                        <p className="bottom-desc">
                                            {projectsData[currentProject].bottom_desc.map((desc, index) => (
                                                <span key={index} style={{ display: index === descIndex ? "inline" : "none" }}>
                                                    {desc}<br />
                                                </span>
                                            ))}
                                        </p>
                                    )}
                                </div>}
                            </div>
                        )}
                    </div>
                    <div className="proj-buttons">
                        <div className="empty-cell"></div>
                        <div className="prev" onClick={() => handleProjectChange("prev")}>
                            {projectsData[prevProjectIndex].path && (
                                <img
                                    className="circular-button"
                                    src={projectsData[prevProjectIndex].path}
                                    alt={projectsData[prevProjectIndex].title}
                                />
                            )}
                        </div>
                        <div className="next" onClick={() => handleProjectChange("next")}>
                            {projectsData[nextProjectIndex].path && (
                                <img
                                    className="circular-button"
                                    src={projectsData[nextProjectIndex].path}
                                    alt={projectsData[nextProjectIndex].title}
                                />
                            )}
                        </div>
                        <div className="empty-cell"></div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Projects;
