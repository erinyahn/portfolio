import React from "react";
import erin from "./img/headshot.png";
import { Link as ScrollLink } from "react-scroll";
import CV from './CV.jsx'
import "./App.css";
import "./Intro.css";

const Intro = () => {
    return (
        <section>
        <div className="container-about flex-container">
            <img
            src={erin}
            className="pro-pic"
            style={{
                objectFit: "cover",
            }}
            alt="headshot"
            />
            <div className="text-container">
            <h1 className="h1">HI, I'M ERIN</h1>
            <p className="intro-desc text-change" style={{ fontSize: "70%" }}>
                I enjoy crafting{" "}
                <strong style={{ fontWeight: "bold" }}>
                intuitive user experiences
                </strong>{" "}
                and{" "}
                <strong style={{ fontWeight: "bold" }}>
                solving complex challenges
                </strong>{" "}
                through code.
            </p>
            <div className="intro_buttons">
                <CV />
                <ScrollLink
                    to="contact-section"
                    smooth={true}
                    offset={-70}
                    duration={500}
                    style={{ marginLeft: '20px' }}
                >
                    <button className="button">
                    LET'S CONNECT
                    <div className="button__horizontal"></div>
                    <div className="button__vertical"></div>
                    </button>
                </ScrollLink>
            </div>
            <div style={{ marginBottom: "35px" }}></div>
            </div>
        </div>
        </section>
    );
};

export default Intro;
