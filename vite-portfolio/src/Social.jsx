import React from "react";
import "./Social.css";

const Social = () => {
    return (
        <div className="soc">
            <a href="https://gitlab.com/erinyahn/" className="icon-button gitlab" target="_blank">
                <i className="fab fa-gitlab"></i>
                <span></span>
            </a>

            <a href="https://github.com/erinyahn/" className="icon-button github" target="_blank">
                <i className="fab fa-github"></i>
                <span></span>
            </a>

            <a href="https://www.linkedin.com/in/erinyahn" className="icon-button linkedin" target="_blank">
                <i className="fab fa-linkedin"></i>
                <span></span>
            </a>
        </div>
    );
};

export default Social;
