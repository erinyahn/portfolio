import React, { useState } from "react";
import "./Contact.css";
import "./Skills.css";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const Skills = () => {
    const skillsData = [
        { name: "Python", icon: "https://www.svgrepo.com/show/452091/python.svg" },
        { name: "JavaScript", icon: "https://www.svgrepo.com/show/452045/js.svg" },
        { name: "HTML5", icon: "https://www.svgrepo.com/show/452228/html-5.svg" },
        { name: "CSS", icon: "https://www.svgrepo.com/show/452185/css-3.svg" },
        {
        name: "Bootstrap",
        icon: "https://www.svgrepo.com/show/353498/bootstrap.svg",
        },
        {
        name: "Django",
        icon: "https://www.svgrepo.com/show/353657/django-icon.svg",
        },
        { name: "Docker", icon: "https://www.svgrepo.com/show/452192/docker.svg" },
        {
        name: "FastAPI",
        icon: "https://cdn.worldvectorlogo.com/logos/fastapi-1.svg",
        },
        { name: "React", icon: "https://www.svgrepo.com/show/354259/react.svg" },
        { name: "Vite", icon: "https://www.svgrepo.com/show/374167/vite.svg" },
        {
        name: "PostgreSQL",
        icon: "https://www.svgrepo.com/show/354200/postgresql.svg",
        },
        {
        name: "MongoDB",
        icon: "https://www.svgrepo.com/show/331488/mongodb.svg",
        },
    ];

    const chunkArray = (array, chunkSize) => {
        const result = [];
        for (let i = 0; i < array.length; i += chunkSize) {
        result.push(array.slice(i, i + chunkSize));
        }
        return result;
    };

    const skillChunks = chunkArray(skillsData, 4);

    const [displayCarousel, setDisplayCarousel] = useState(true);

    const toggleDisplay = () => {
        setDisplayCarousel(!displayCarousel);
    };

    return (
        <section>
        <div className="skills-container flex-container">
            <div className="skills-content">
            <h2>SKILLS</h2>
            <p className="skills-desc" onClick={toggleDisplay}>
                {displayCarousel ? (
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    height="16"
                    width="14"
                    viewBox="0 0 448 512"
                >
                    <path d="M201.4 342.6c12.5 12.5 32.8 12.5 45.3 0l160-160c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L224 274.7 86.6 137.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l160 160z" />
                </svg>
                ) : (
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    height="16"
                    width="14"
                    viewBox="0 0 448 512"
                >
                    <path d="M201.4 137.4c12.5-12.5 32.8-12.5 45.3 0l160 160c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L224 205.3 86.6 342.6c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3l160-160z" />
                </svg>
                )}
                <span className="text-change">
                &nbsp;{displayCarousel ? "Display All" : "Show Preview"}
                </span>
            </p>
            {displayCarousel ? (
                <div className="skills-carousel">
                <Carousel
                    showArrows={true}
                    showThumbs={false}
                    showStatus={false}
                    emulateTouch
                    infiniteLoop
                    autoPlay={true}
                    interval={1500}
                >
                    {skillChunks.map((chunk, index) => (
                    <div key={index} className="carousel-item">
                        {chunk.map((skill, skillIndex) => (
                        <div key={skillIndex} className="custom-skill-item">
                            <img src={skill.icon} alt={skill.name} />
                            <p className="text-change" style={{ fontSize: "75%" }}>
                            {skill.name}
                            </p>
                            <div style={{ marginBottom: "35px"}}></div>
                        </div>
                        ))}
                    </div>
                    ))}
                </Carousel>
                </div>
            ) : (
                <div className="expanded-skills">
                {skillsData.map((skill, index) => (
                    <div key={index} className="custom-skill-item">
                    <img src={skill.icon} alt={skill.name} />
                    <p className="skills-name text-change">
                        {skill.name}
                    </p>
                    </div>
                ))}
                </div>
            )}
            <p className="skills-desc text-change">
                👀 Don't see a specific skill? No worries, I'm always up for learning new skills! 👩🏻‍💻            </p>
            </div>
        </div>
        </section>
    );
};

export default Skills;
