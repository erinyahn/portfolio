import React from "react";
import resume from "./assets/resume.pdf";
import "./CV.css"; 

const CV = () => {
    return (
        <div className="cv">
            <a target="_blank" rel="noreferrer" href={resume} className="icon-button cv-button">
                <i className="far fa-file-pdf"></i>
                <span></span>
            </a>
        </div>
    );
};

export default CV;
