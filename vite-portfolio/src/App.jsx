import React, { useEffect, useRef, useState } from "react";
import Projects from "./Projects.jsx";
import Intro from "./Intro.jsx";
import Contact from "./Contact.jsx";
import Skills from "./Skills.jsx";
import Sidebar from "./Sidebar.jsx";
import AutoDealersMov from "./assets/AutoDealers.mp4";
import PrestigePalateMov from "./assets/PrestigePalate.mp4";
import pygameMov from "./assets/pygame.mp4";
import AutoDealers from "./assets/AutoDealers.png";
import PrestigePalate from "./assets/PrestigePalate.png";
import pygame from "./assets/pygame.png";
import Scroll from "./Scroll.jsx";

const App = () => {
  const [currentProject, setCurrentProject] = useState(0);
  const [isSidebarOpen, setSidebarOpen] = useState(false);
  const mainDivRef = useRef(null);
  const [isTop, setIsTop] = useState(true);

  useEffect(() => {
    const handleScroll = (e) => {
      const scrollTop = window.scrollY;
      const isTop = scrollTop === 0;

      setIsTop(isTop);
    };

    const handleResize = () => {
      const isSmallScreen = window.innerWidth <= 768;

      if (isSmallScreen) {
        window.addEventListener("scroll", handleScroll);
        window.addEventListener("keydown", handleScroll);
      } else {
        window.removeEventListener("scroll", handleScroll);
        window.removeEventListener("keydown", handleScroll);
      }
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
      window.removeEventListener("scroll", handleScroll);
      window.removeEventListener("keydown", handleScroll);
    };
  }, []);

  const handleProjectChange = (direction) => {
    setCurrentProject((prevProject) => {
      const lastIndex = projectsData.length - 1;
      if (direction === "next") {
        return prevProject === lastIndex ? 0 : prevProject + 1;
      } else if (direction === "prev") {
        return prevProject === 0 ? lastIndex : prevProject - 1;
      }
      return prevProject;
    });
  };

  const handleKeyDown = (e) => {
    if (e.key === "ArrowUp" || e.key === "ArrowDown" || e.key === " ") {
      e.preventDefault();
    }
  };

  const closeSidebar = () => {
    setSidebarOpen(false);
  };

  const projectsData = [
    {
      title: "DODGE GAME",
      api: null,
      description: "Simple dodge game implemented in Python using Pygames.",
      path: pygame,
      mov: pygameMov,
      icons: [
        "https://www.pygame.org/docs/_static/pygame_logo.svg",
        "https://www.svgrepo.com/show/452091/python.svg",
      ],
      code_link: "https://gitlab.com/erinyahn/dodge",
      live_link: null,
      bottom_desc: [
        "You'll see a counter of how many objects the player dodged on the left top corner",
        "and the number of lives remaining on the top right.",
        "The player gains an extra life when they come in contact with the red potion appearing on the ground at random intervals.",
      ],
    },
    {
      title: "AUTO DEALERS",
      api: null,
      description: "Automobile Dealership Management System.",
      path: AutoDealers,
      mov: AutoDealersMov,
      icons: [
        "https://www.svgrepo.com/show/452091/python.svg",
        "https://www.svgrepo.com/show/452045/js.svg",
        "https://www.svgrepo.com/show/353498/bootstrap.svg",
        "https://www.svgrepo.com/show/353657/django-icon.svg",
        "https://www.svgrepo.com/show/354259/react.svg",
        "https://www.svgrepo.com/show/452192/docker.svg",
        "https://www.svgrepo.com/show/354200/postgresql.svg",
      ],
      code_link: "https://gitlab.com/erinyahn/auto-dealer",
      live_link: null,
      bottom_desc: [
        "Create Form for adding new cars to the database.",
        "Form to input manufacturers, models, and automobiles and a list view of all three.",
        "Form to input new salespeople, customers, and technician and a user-friendly list view of all three.",
        "Filter sales history by specific salesperson in list view.",
        "Form to create new service request and a list view of all service requests.",
        "Cancel and Finish buttons to change the status of service requests.",
        "Search function on Service History to search by exact VIN number.",
        "Feature to identify VIP based on if the customer has purchased the vehicle with AutoDealers.",
      ],
    },
    {
      title: "PRESTIGE PALATE",
      api: "Amazon S3, and Google Maps Geocoding and Places API",
      description:
        "Restaurant Search, Review, and Rating Web Application.",
      path: PrestigePalate,
      mov: PrestigePalateMov,
      icons: [
        "https://www.svgrepo.com/show/452091/python.svg",
        "https://www.svgrepo.com/show/452045/js.svg",
        "https://www.svgrepo.com/show/354259/react.svg",
        "https://www.svgrepo.com/show/374167/vite.svg",
        "https://www.svgrepo.com/show/353498/bootstrap.svg",
        "https://cdn.worldvectorlogo.com/logos/fastapi-1.svg",
        "https://www.svgrepo.com/show/354200/postgresql.svg",
        "https://www.svgrepo.com/show/452192/docker.svg",
      ],
      code_link: "https://gitlab.com/mambo-number-5/prestige-palate",
      live_link: "https://mambo-number-5.gitlab.io/prestige-palate/",
      bottom_desc: [
        "This web app uses a referral-only sign-up system to ensure a trusted user community.",
        "To navigate, click on the live link and log in using admin@emaill.com and string for the password.",
        "Follow/unfollow user system for enhanced user interaction and connection.",
        "Dashboard of reviews and ratings from users you follow.",
        "User-friendly platform for global restaurant discovery with Google reviews + photos and reviews from other users.",
        "Create Review with Rating and Photos for authenticated users to share dining experiences.",
      ],
    },
  ];

  return (
    <>
      <div style={{ marginTop: "70px" }}></div>
      <Scroll ref={mainDivRef} />
      <div style={{ marginTop: "70px" }}></div>
      <div className="mainpage">
        <div className="mainbody">
          {isTop && <Sidebar closeSidebar={closeSidebar} />}
          <div id="intro-section">
            <Intro />
          </div>
          <div style={{ marginTop: "50px" }}></div>
          <div id="skills-section">
            <Skills />
          </div>
          <div style={{ marginTop: "50px" }}></div>
          <div id="projects-section">
            <Projects
              projectsData={projectsData}
              currentProject={currentProject}
              handleProjectChange={handleProjectChange}
            />
          </div>
          <div style={{ marginTop: "50px" }}></div>
          <div id="contact-section">
            <Contact />
          </div>
        </div>
      </div>
      <div style={{ marginBottom: "75px" }}></div>
    </>
  );
};

export default App;
