import React, { useState, useEffect } from "react";
import "./Character.css";
import leftStand from "./sprites/left-stand.png";
import leftWalk from "./sprites/left-walk.png";
import leftSit from "./sprites/left-sit.png";
import leftFly from "./sprites/left-fly.png";
import rightStand from "./sprites/right-stand.png";
import rightWalk from "./sprites/right-walk.png";
import rightSit from "./sprites/right-sit.png";
import rightFly from "./sprites/right-fly.png";


const Character = () => {
    const initialYPosition = window.innerWidth >= 1400 ? -100 : -70;
    const initialXPosition = window.innerWidth >= 1400 ? 20 : 0;
    const jumpHeight = window.innerWidth >= 1400 ? -12 : -7;
    const runSpeed = window.innerWidth >= 1400 ? 20 : 10;
    const maxRight = window.innerWidth >= 1400 ? 80 : 65;

    const [characterPosition, setCharacterPosition] = useState({
        x: initialXPosition,
        y: initialYPosition,
    });
    const [characterImage, setCharacterImage] = useState(leftStand);
    const [velocity, setVelocity] = useState({ x: 0, y: 0 });
    const gravity = 1;

    useEffect(() => {
        const handleKeyDown = (event) => {
        const windowWidth = window.innerWidth;

        switch (event.key) {
            case "ArrowRight":
                setCharacterPosition((prevPosition) => ({
                    ...prevPosition,
                    x: Math.min(prevPosition.x + runSpeed, windowWidth/2 + maxRight),
                }));
                setCharacterImage(leftWalk);
                break;
            case "ArrowLeft":
                setCharacterPosition((prevPosition) => ({
                    ...prevPosition,
                    x: Math.max(prevPosition.x - runSpeed, 0),
                }));
                setCharacterImage(rightWalk);
                break;
            case "ArrowDown":
                if (characterPosition.y === initialYPosition) {
                    setVelocity((prevVelocity) => ({ ...prevVelocity, y: +5 })); 
                }
                if (characterImage === leftStand) {
                    setCharacterImage(leftSit);
                } else if (characterImage === rightStand) {
                    setCharacterImage(rightSit);
                }
                break;
            case "ArrowUp":
                if (characterPosition.y === initialYPosition) {
                    setVelocity((prevVelocity) => ({ ...prevVelocity, y: jumpHeight })); 
                }
                if (characterImage === leftStand) {
                    setCharacterImage(leftFly);
                } else if (characterImage === rightStand) {
                    setCharacterImage(rightFly);
                }
                break;
            default:
            break;
        }
        };

        const handleKeyUp = (event) => {
            switch (event.key) {
                case "ArrowRight":
                    setCharacterImage(leftStand);
                    break;
                case "ArrowLeft":
                    setCharacterImage(rightStand);
                    break;
                case "ArrowDown":
                    if (characterImage === leftSit) {
                        setCharacterImage(leftStand);
                    } else if (characterImage === rightSit) {
                        setCharacterImage(rightStand);
                    }
                    break;
                case "ArrowUp":
                    if (characterImage === leftFly) {
                        setCharacterImage(leftStand);
                    } else if (characterImage === rightFly) {
                        setCharacterImage(rightStand);
                    }
                    break;
                default:
                    break;
        }
        };

        const updateCharacter = () => {
        setVelocity((prevVelocity) => ({ ...prevVelocity, y: prevVelocity.y + gravity }));

        setCharacterPosition((prevPosition) => ({
            x: Math.min(Math.max(prevPosition.x + velocity.x, 0), window.innerWidth),
            y: Math.min(Math.max(prevPosition.y + velocity.y, -500), initialYPosition),
        }));
        };

        const animationFrame = requestAnimationFrame(() => {
        updateCharacter();
        });

        window.addEventListener("keydown", handleKeyDown);
        window.addEventListener("keyup", handleKeyUp);

        return () => {
        window.removeEventListener("keydown", handleKeyDown);
        window.removeEventListener("keyup", handleKeyUp);
        cancelAnimationFrame(animationFrame);
        };
    }, [velocity, characterPosition, initialYPosition]);

    const isMobile = window.innerWidth <= 600;
    const isLargeScreen = window.innerWidth >= 1400;
    const scaleFactor = isMobile ? 0.65 : isLargeScreen ? 1.5 : 1;

    return (
        <div
        className="Character"
        style={{
            transform: `translate(${characterPosition.x}px, ${characterPosition.y}px) scale(${scaleFactor})`,
        }}
        >
        <img
            className="Character_spritesheet pixelart"
            src={characterImage}
            alt="Character"
            style={{
            animation: "moveSpritesheet 1s steps(12) infinite",
            }}
        />
        </div>
    );
};

export default Character;
