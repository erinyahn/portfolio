import React, { useState } from "react";
import { Link as ScrollLink } from "react-scroll";
import "./Sidebar.css";
import Social from "./Social";

const Sidebar = ({ closeSidebar }) => {
    const [isSidebarOpen, setSidebarOpen] = useState(false);

    const toggleSidebar = () => {
        setSidebarOpen(!isSidebarOpen);
    };

    const closeSidebarAndHandleItemClick = () => {
        setSidebarOpen(false);
        closeSidebar(); 
    };

    return (
        <>
        <nav className="navbar">
            <div className="menu-icon" onClick={toggleSidebar}>
            ☰
            </div>
            <div className={`sidebar ${isSidebarOpen ? "open" : ""}`}>
            <div className="close-icon" onClick={toggleSidebar}>
                ✕
            </div>
            <div className="upper-level">
                <ScrollLink
                activeClass="active"
                to="intro-section"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
                onClick={closeSidebarAndHandleItemClick}
                >
                <div className="sidebar-item">Intro</div>
                </ScrollLink>
                <ScrollLink
                activeClass="active"
                to="skills-section"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
                onClick={closeSidebarAndHandleItemClick}
                >
                <div className="sidebar-item">Skills</div>
                </ScrollLink>
                <ScrollLink
                activeClass="active"
                to="projects-section"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
                onClick={closeSidebarAndHandleItemClick}
                >
                <div className="sidebar-item">Projects</div>
                </ScrollLink>
                <ScrollLink
                activeClass="active"
                to="contact-section"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
                onClick={closeSidebarAndHandleItemClick}
                >
                <div className="sidebar-item">Contact</div>
                </ScrollLink>
            </div>
            <div className="lower-level">
                <Social />
            </div>
            </div>
            </nav>
        </>
    );
};

export default Sidebar;
