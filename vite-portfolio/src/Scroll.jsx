import React, { forwardRef, useEffect } from "react";

const Scroll = forwardRef((props, ref) => {
    useEffect(() => {
        const handleScroll = (e) => {
            const focusedElement = document.activeElement;
            if (
                focusedElement.tagName.toLowerCase() !== "input" &&
                focusedElement.tagName.toLowerCase() !== "textarea"
            ) {
                if (e.key === "ArrowUp" || e.key === "ArrowDown" || e.key === " ") {
                    e.preventDefault();
                }
            }
        };

        window.addEventListener("keydown", handleScroll);

        return () => {
            window.removeEventListener("keydown", handleScroll);
        };
    }, []);

    return <div tabIndex="0" ref={ref} {...props}></div>;
});

export default Scroll;
