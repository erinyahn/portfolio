import React, { useState } from "react";
import Social from "./Social";
import "./Contact.css";

const Contact = () => {
    const initialFormData = {
        firstName: "",
        lastName: "",
        email: "",
        message: "",
    };

    const [formData, setFormData] = useState(initialFormData);
    const [submitting, setSubmitting] = useState(false);
    const [submissionStatus, setSubmissionStatus] = useState(null);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevData) => ({
        ...prevData,
        [name]: value,
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setSubmitting(true);

        try {
        const response = await fetch("https://formspree.io/f/xyyrglee", {
            method: "POST",
            headers: {
            "Content-Type": "application/json",
            },
            body: JSON.stringify(formData),
        });

        if (response.ok) {
            setSubmissionStatus("success");
            setTimeout(() => {
            setSubmissionStatus(null);
            }, 3000);
        } else {
            setSubmissionStatus("error");
        }
        } catch (error) {
        console.error("Error submitting form:", error);
        setSubmissionStatus("error");
        } finally {
        setSubmitting(false);
        setFormData(initialFormData);
        }
    };


    return (
        <section>
        <div className="form-container">
            <div className="form-content">
            <h2>CONTACT ME</h2>
            <div style={{ marginBottom: "10px" }}></div>

            <form onSubmit={handleSubmit}>
                <div className="name-fields text-change">
                <div className="name-field">
                    <label>
                    First Name:
                    <input
                        type="text"
                        name="firstName"
                        value={formData.firstName}
                        onChange={handleChange}
                        required
                    />
                    </label>
                </div>
                <div className="name-field">
                    <label>
                    Last Name:
                    <input
                        type="text"
                        name="lastName"
                        value={formData.lastName}
                        onChange={handleChange}
                        required
                    />
                    </label>
                </div>
                </div>
                <div className="email-field text-change">
                <label>
                    Email Address:
                    <input
                    type="email"
                    name="email"
                    value={formData.email}
                    onChange={handleChange}
                    required
                    />
                </label>
                </div>
                <div className="message-field text-change">
                <label>
                    Message:
                    <textarea
                    name="message"
                    className="message"
                    value={formData.message}
                    onChange={handleChange}
                    required
                    ></textarea>
                </label>
                </div>
                <button
                className="button rounded"
                type="submit"
                disabled={submitting}
                >
                {submitting ? "Submitting..." : "Submit"}
                </button>
                {submissionStatus && (
                <div
                    className="text-change"
                    style={{
                    textAlign: "center",
                    backgroundColor:
                        submissionStatus === "success" ? "green" : "red",
                    color: "white",
                    padding: "10px",
                    borderRadius: "8px",
                    }}
                >
                    <p>
                    {submissionStatus === "success"
                        ? "📧 Message sent successfully!"
                        : "😵‍💫 Oops! Something went wrong. Please try again later."}
                    </p>
                </div>
                )}
            </form>
            </div>
            <Social />
        </div>
        </section>
    );
};

export default Contact;
