import React from "react";
import "./App.css";
import "./NPC.css";
import npc from "./img/npc.png";
import start from "./img/start.png";

const NPC = ({ onStartClick }) => {
    const handleStartClick = () => {
        onStartClick();
    };

    return (
        <div className="npc" >
            <div className="npc-image-and-name">
                <div className="npc-image">
                <img
                    src={npc}
                    style={{
                        objectFit: "cover",
                        width: "60%",
                    }}
                    alt="npc"
                />
                </div>
                <div className="npc-name">Erin</div>
            </div>
            <div className="npc-body">
                <div>
                Hey - Glad you made it down here.
                Soon you'll see some video demos of my projects in action. 
                You can always hover over the screen to get more info,
                but I'll be around to expand on the project features.
                Try the arrow keys on the console/keyboard to move me around and
                if I'm in the way, move me out of the screen by taking me to the far right.
                Go ahead and click the start button to get started.
                </div>
                <div className= "empty-area"></div>
                <img
                    src={start}
                    className="start"
                    alt="start"
                    onClick={handleStartClick}
                />
            </div>
        </div>
    );
};

export default NPC;
